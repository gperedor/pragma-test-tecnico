# Pragma Technical Test

This repository contains a solution for Pragma's technical test as outlined in
```EnunciadoEvaluacionJEE.txt```.

## Requirements

### External dependencies

This application requires the following to be installed:

- Java 8
- Maven

Plus a suitable Postgres instance.

### Database configuration

This software assumes that a suitable Postgres DB and a user with all privileges
has been assigned to it.

One way of achieving this would be by issuing:

```
create database banco;
create user operador with password 'devpass’;
grant all privileges on database banco to operador;
```

## Configuration

The application is configured via environment variables. The required variables are found
on the env.example file. An example below is given for Unix systems on how to accomplish this.

## Running

Having set the environment variables, issue

```
mvn clean install && java -jar target/pragma-0.0.1-SNAPSHOT.jar
```

You can do both in a single step if you have prepared a file like ```env.example``` like
this:

```
(export $(xargs < ./.env); mvn clean install && java -jar target/pragma-0.0.1-SNAPSHOT.jar)
```

Which sets the variables in a subshell, as to not pollute the enclosing namespace.

This will launch an embedded Tomcat server on port 8080, which you can access via the
```http://127.0.01:8080/resources/index.html``` URL.

