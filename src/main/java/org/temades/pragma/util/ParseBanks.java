package org.temades.pragma.util;

import org.temades.pragma.model.Bank;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author gabrielperedo
 */
public class ParseBanks {

    /**
     * Parses an InputStream for CSV data into a Bank list
     * @param csv
     * @return
     */
    public static List<Bank> parseCSV(InputStream csv) {
        List<Bank> banks = new ArrayList<>();
        Stream<String> lines = new BufferedReader(new InputStreamReader(csv,
                StandardCharsets.UTF_16)).lines().skip(1);
        for (String l : lines.collect(Collectors.toList())) {
            // The files often have empty lines in between, they are useless
            if (l.trim().length() == 0) continue;
            String[] fields = l.split(",");
            banks.add(new Bank(fields[0], fields[1]));
        }
        return banks;
    }

    /**
     * Parses an InputStream for an XML document into a Bank list.
     * The expected structure is:
     * <bancos>
     *   <banco>
     *      <bco_banco_id>0001</bco_banco_id>
     *      <bco_banco_descripcion_ds>BANCO DE CHILE</bco_banco_descripcion_ds>
     *   </banco>
     * </banco>
     *
     * @param xml
     * @return
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public static List<Bank> parseXML(InputStream xml) throws ParserConfigurationException, IOException, SAXException {
        List<Bank> banks = new ArrayList<>();
        DocumentBuilderFactory dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xml);
        doc.getDocumentElement().normalize();
        System.out.println("Root element :"
                + doc.getDocumentElement().getNodeName());

        NodeList bankElements = doc.getElementsByTagName("banco");
        int inputSize = bankElements.getLength();
        for (int i = 0; i < inputSize; i++) {
            Element bankElement = (Element) bankElements.item(i);
            String cod = bankElement.getElementsByTagName("bco_banco_id").item(0).getTextContent();
            String desc = bankElement.getElementsByTagName("bco_banco_descripcion_ds").item(0).getTextContent();
            banks.add(new Bank(cod, desc));
        }
        return banks;
    }

}
