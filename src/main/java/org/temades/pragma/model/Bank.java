package org.temades.pragma.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author gabrielperedo
 */
@Entity
@Table(name = "prg_banco")
public class Bank {
    @Id
    private String cBanco;
    private String dBanco;

    public Bank() {

    }

    public Bank(String cBanco, String dBanco) {
        this.cBanco = cBanco;
        this.dBanco = dBanco;
    }

    public String getcBanco() {
        return cBanco;
    }

    public void setcBanco(String cBanco) {
        this.cBanco = cBanco;
    }

    public String getdBanco() {
        return dBanco;
    }

    public void setdBanco(String dBanco) {
        this.dBanco = dBanco;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "cBanco='" + cBanco + '\'' +
                ", dBanco='" + dBanco + '\'' +
                '}';
    }
}
