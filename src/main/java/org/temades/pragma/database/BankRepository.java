package org.temades.pragma.database;

import org.springframework.data.repository.CrudRepository;
import org.temades.pragma.model.Bank;

/**
 * @author gabrielperedo
 */
public interface BankRepository extends CrudRepository<Bank, String> {
}
