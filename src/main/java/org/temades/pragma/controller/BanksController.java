package org.temades.pragma.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.temades.pragma.database.BankRepository;
import org.temades.pragma.model.Bank;
import org.temades.pragma.util.ParseBanks;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.StreamSupport;


/**
 * @author gabrielperedo
 */
@RestController
public class BanksController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private BankRepository repository;

    /**
     * Simple list of banks taken from the database.
     * @return The list of all banks.
     */
    @RequestMapping(value = "/banks", method = RequestMethod.GET)
    public List<Bank> getBanks() {
        ArrayList<Bank> l = new ArrayList<>();
        repository.findAll().forEach(l::add);
        return l;
    }

    /**
     * FormData-based file upload method for XML or CSV files
     * containing Bank entries.
     * @param request
     * @return Number of entities loaded
     */
    @RequestMapping(value = "/banks", method = RequestMethod.POST)
    public Long ajaxUpload(MultipartHttpServletRequest request) {
        System.out.println(request.getRequestHeaders());
        Iterator<String> files = request.getFileNames();
        String fileParam = files.next();

        // This signals that the file is missing
        MultipartFile file = request.getFile(fileParam);
        String fileName = file.getOriginalFilename();
        if (fileName.length() == 0) {
            logger.info("Empty file upload");
            return 0l;
        }

        String[] parts = fileName.split("\\.");
        String extension = parts[parts.length - 1];
        List<Bank> banks = new ArrayList<>();
        if (extension.equals("csv")) {
            try (InputStream csv = file.getInputStream()) {
                banks = ParseBanks.parseCSV(csv);
            } catch (IOException e) {
                logger.error("Could not open file input stream", e);
            }
        } else if (extension.equals("xml")) {
            try (InputStream xml = file.getInputStream())  {
                banks = ParseBanks.parseXML(xml);
            } catch (IOException e) {
                logger.error("Could not open file input stream", e);
            } catch (ParserConfigurationException e) {
                logger.error("Could not create XML parser", e);
            } catch (SAXException e) {
                logger.error("Could not parse XML document", e);
            }
        }
        Iterable<Bank> savedBanks = repository.save(banks);
        logger.info("Saved " + savedBanks + " bank records");
        return savedBanks.spliterator().estimateSize();
    }
}
