# Implementation of technical test

This application was written using Spring Boot, serving static files
which interact with the server via AJAX.

The application is organized in four layers, following something like
the MVC pattern

- Model: containing the Bank entity mapped to the database
- Repository: using Spring's built-in facilities for generic data access.
- Controller: orchestrating the fetching and loading of data.
- View: static HTML files using the DataTables library.

DB schema creation (and updates) is done via Flyway, which executes the
migration contained in ```/src/main/resources/db/migration/V1__create_PRG_BANCO.sql```.
